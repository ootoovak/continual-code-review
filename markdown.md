layout: true
class: middle, center

.twitter[@ootoovak]

---

# Continual Code Review

**Samson Ootoovak**

Developer at Flick Electric

???
Hello I am Samson Ootoovak

---

# Pairing

???
I am here to talk to you today about pairing

---

# Continual Code Review

???
Or Continual Code Reivew

---

# ~~Continual Code Review~~
# Continual Rubber Ducking

---


# ~~Continual Code Review~~
# ~~Continual Rubber Ducking~~
# Continual Design Meeting

---

# ~~Continual Code Review~~
# ~~Continual Rubber Ducking~~
# ~~Continual Design Meeting~~
## You get the idea...

---

layout: true
class: top, left

.twitter[@ootoovak]

---

# The Hook

You may have heard about

*continuous integration*,

*continuous deployment*,

all the *continuous* things.

--

But what about the *continuous people* stuff?

---

# Pairing!

- What is pairing even?
- Why am I talking about it?
- What does pairing have to offer?
- How do I even?
- Things to look out for.
- And some helpful quotes!

???
So here's the plan

---

# Please tell me about pairing?

- In this talk when I say Pairing I mean Pair Programming.
--

- Some people call it Peer Programming but most use Pair (as in two).
--

- Pairing came out of Agile software development.

???
The history is interesting but...

---

# When you are Pair Programming there are a few key elements.

---

# Setup

## Equipment:

- One computer
- Sometimes, one mouse, one keyboard.
- But sometimes one mouse and keyboard each connected to the same computer.
- Big or dual monitors helps.

???
Lets talk about the equipment you need before we get into the more important people stuff.

---

# Setup

## There are traditionally two people:

- One "**driver**"
- One "**navigator**"

???
So lets move on to people side of things

---

# Driver

Is the **only** person controlling the computer.
- They are typing in the code.

--

- Or... looking up docs or error messages online.

--

- Also, their focus is communicating their thoughts with their pair.

---

# Navigator

The person thinking about what is going on and what is coming up.
- *Not controlling the computer.*

--

- They are trying to think understand what the driver is doing at all times.

--

- Also, their focus is communicating their thoughts with their pair.

---

background-image: url(images/ncis-pairing.gif)

---

layout: true
class: middle, center

.twitter[@ootoovak]

---

*Driver:* Their focus is communicating their thoughts with their pair.

*Navigator:* Their focus is communicating their thoughts with their pair.

---

*Driver:* Their focus is **communicating** their thoughts with their pair.

*Navigator:* Their focus is **communicating** their thoughts with their pair.

---

class: quotes

> “Communicate! If you have never paired before, always check in with your partner at the beginning, during and after.”

*Olivia Baddeley*

<small>
  Developer, Flick Electric - @baddjuice
</small>

---

# That's pretty much it.
There are a bunch of other ~~rules~~ suggestions about how to do it but I think it is largely up to your team, your pair, on what works best for all of you.

---

# My experience
## AKA "Why should I listen to you?"

???
- Didn't do pairing for a long time at the start of my career
- Did some pairing but not much
- Went to Dev Bootcamp in San Fransisco to train to be a bootcamp
  instructor
- Did pairing there
- Then did pairing and live coding with students
- Started doing some part time consulting
- Training up two Jr Developers and pairing with them about half the
  time
- Now work at Flick Electric and have paired with Juiors on project
- And more recently with the other Seniors
- Want to make it more of a thing

---

layout: true
class: middle, center, benefits

.twitter[@ootoovak]

---

# Benefits

---

layout: true
class: top, left, benefits

.twitter[@ootoovak]

---

# Organisation PoV

Two developers on one computer isn't that a waste?

---

layout: true
class: middle, center, benefits

.twitter[@ootoovak]

---

# Benefits - Organisation

.plus[It's Teams++]

--

class: teams

background-image: url(images/teams-plus-plus.gif)


???
- Instant code review
- Context aware
- Less code ownership
- Get unstuck
- Fewer rabbit holes
- Upskill faster
- Shared experince (team stuff)
- There are some numbers on online

---

class: quotes

> “Think of it like rubber ducking, but the rubber duck responds to you, asks questions and points out things that you miss.”

*Cara Hill*

<small>
  Developer, Rabid Technologies - @ilikeprettycode
</small>

---

layout: true
class: top, left

.twitter[@ootoovak]

---

# So how do you do it?

---

layout: true
class: top, left

.twitter[@ootoovak]

---

# Types

First lets quickly talk about types of pairing before we get into the how.

---

# Who are you pairing with?

Because pairing involves other people (gasp!) who you are pairing with should be on your mind.

???
- Pairing with newer that you
- Pairing with someone at your level
- Pairing with someone more experienced that you
- Pairing with non or part time programmers

---

layout: true
class: middle, center

.twitter[@ootoovak]

---

class: quotes

> “I can't Objective-C for jam, but we had a great session with him explaining Objective-C to me and me explaining patterns and things to him.”

*Ben Amor*

<small>
  CTO, ShowGizmo - @shortmethods
</small>

---

# Strategies

---

# Start slow

???
It is like exercise.

---

# Check in

???
The warm up, like stretching
Before and after

---

# Setting up expectations

???
- For this sesession
- A head of time for future sessions
- Maybe how you have done pairing before, if at all

---

class: quotes

> “Talk at the beginning and discuss expectations and what each person wants to get out of the experience.”

*Libby Schumacher-Knight*

<small>
  Developer, Flick Electric - @schuknight
</small>

---

# Take frequent breaks

---

# Find non-pairing tasks

???
Finding a nice rhythm in tasks and goals

---

# Close quarters

???
- Deoderant
- Gum

---

# Talk all the time

???
- If you don't konw what you are doing say so.
- Speak out loud about how you are thinking it might be solved.

---

class: quotes

> “Embrace the awkward silence. If you wait an extra 30 seconds, you might be surprised and delighted with what your pairing partner comes up with.”

*Raquel Moss*

<small>
  Freelance Developer - @raquelxmoss
</small>

---

# Practice

???
- It's worth it.

---

layout: true
class: middle, center, difficulties

.twitter[@ootoovak]

---

# Difficulties to watch for

---

# It is a completely different and often new skill!

---

# Learning to be Vulnerable

???
- As a Jr feeling you don't know enough (or anything!)
- Making the Jr problems your problems
- As a Sr feeling like you don't know enough!
- As a Sr being able to admit when you don't know things.

---

# Tiring

???
Like exercise start slow.

---

# Development Styles

???
Jumping around vs linar style

---

# Exploring

???
Can feel harder when you have no idea and it's all about the research or reading

---

# Business Time Pressures

???
The feeling when you just need to get something out the door quickly

---

layout: true
class: middle, center, anti-patterns

.twitter[@ootoovak]

---

# Anti-patterns

---

# Pairing does not mean grabbing the keyboard when you have an idea or understand some concept first

---

background-image: url(images/ncis-pairing.gif)

---

# It also does not mean checking your texts while someone else types

---

layout: true
class: middle, center, benefits

.twitter[@ootoovak]

---

# Why put yourself through it?

---

# Benefits - Personal

## Learning

???
- Doesn't matter from what level
- Upskill faster
- More insight

---

layout: true
class: middle, center

.twitter[@ootoovak]

---

class: quotes

> “Be open-minded to what they say because they could get you out of a hole in a way that you weren't expecting.”

*Rob Edgecombe*

<small>
  Developer, Axos Systems
</small>

---

layout: true
class: top, left

.twitter[@ootoovak]

---

# Wrap Up

## Pairing is not magic

.float-right[![magic](images/magic.gif)]

- Work up to it
- Remember it's about the people
- Celebrate your wins

---

layout: true
class: middle, center

.twitter[@ootoovak]

---

class: quotes

> “Appreciate each other or a high-five when the code works or when the tests go green”

*Vinitha Natarajan*

<small>
  Developer, Rabid Technologies
</small>

---

background-image: url(images/super-high-five.gif)

---

layout: true
class: top, left

.twitter[@ootoovak]

---

# Special thanks:

- Olivia Baddeley
- Cara Hill
- Ben Amor
- Libby Schumacher-Knight
- Raquel Moss
- Rob Edgecombe
- Vinitha Natarajan

---

layout: true
class: middle, center

.twitter[@ootoovak]

---

# **Samson Ootoovak**

ootoovak.com

![flick-logo](images/flick-logo.png)
