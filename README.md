# Continual Code Review

Pairing, how does it work? For the often touted solution to your
programming woes and a panacea of perfectly curated code there doesn't
seem to be much to read or listen to about it. In a day-to-day setting
of just trying to get your work done will pairing work for you? In this
talk I'll talk about my experiences at a few different companies pairing
with others and about the things that did and didn't work when trying to
ship quality software as well as the tips about making sure you and your
pair are enjoy coming to into work to program together. In the spirit of
pairing this talk will also not be just the product of one persons point
of view but will also include feedback and tips from the pairs that I
have worked with.

## Starting

To start the presentation locally run the following in the directory
with the `index.html` file.

```bash
./start
```

## Running

Type `c` in the browser window to clone the presentation.

Type `p` in the browser window to see the speaker notes.

## Author

Samson Ootoovak
